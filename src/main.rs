use gtk::{
    prelude::*,
    ApplicationWindow,
    Button,
    Align,
    StyleContext,
    CssProvider,
    gdk::Display
};
use std::process::Command;

fn main() {
    let application = gtk::Application::new(
        Some("rustyreboot.gtk"),
        Default::default()
    );

    application.connect_startup(|_| loadCSS());
    application.connect_activate(buildUI);

    application.run();
}

#[allow(non_snake_case)]
fn loadCSS() {

    let provider = CssProvider::new();
    provider.load_from_data(include_bytes!("style.css"));

    StyleContext::add_provider_for_display(
        &Display::default().expect("Could not connect to a display."),
        &provider,
        gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
    );
}

#[allow(non_snake_case)]
fn createButton(label: &str,
                width: i32,
                height: i32,
                container: &gtk::Box)
    -> gtk::Button {

    let button = Button::builder()
                         .label(label)
                         .width_request(width)
                         .height_request(height)
                         .build();

    container.append(&button);
    return button;
}

#[allow(non_snake_case)]
fn buildUI(application: &gtk::Application) {

    let window = ApplicationWindow::builder()
                                    .application(application)
                                    .title("rustyreboot")
                                    .default_width(400)
                                    .default_height(250)
                                    .build();

    let container = gtk::Box::builder()
                              .orientation(gtk::Orientation::Vertical)
                              .margin_top(20)
                              .margin_bottom(20)
                              .margin_start(20)
                              .margin_end(20)
                              .halign(Align::Center)
                              .valign(Align::Center)
                              .spacing(10)
                              .build();

    let defContainer: &gtk::Box = &container;
    let defWidth:     i32       = 300;
    let defHeight:    i32       = 40;

    createButton("Shutdown", defWidth, defHeight, defContainer).connect_clicked(|_| {
        Command::new("shutdown").arg("now").status().expect("Failed to run command!");
    });

    createButton("Reboot", defWidth, defHeight, defContainer).connect_clicked(|_| {
        Command::new("reboot").status().expect("Failed to run command!");
    });

    createButton("Logout", defWidth, defHeight, defContainer).connect_clicked(|_| {
        Command::new("killall").arg("-q").arg("xmonad-x86_64-linux").status().expect("Failed to run command!");
    });

    window.set_child(Some(&container));

    window.show();
}
